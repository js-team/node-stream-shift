# Installation
> `npm install --save @types/stream-shift`

# Summary
This package contains type definitions for stream-shift (https://github.com/mafintosh/stream-shift).

# Details
Files were exported from https://github.com/DefinitelyTyped/DefinitelyTyped/tree/master/types/stream-shift.
## [index.d.ts](https://github.com/DefinitelyTyped/DefinitelyTyped/tree/master/types/stream-shift/index.d.ts)
````ts
/// <reference types="node"/>

declare function shift(stream: NodeJS.ReadableStream): Buffer | string | null;

export = shift;

````

### Additional Details
 * Last updated: Tue, 07 Nov 2023 15:11:36 GMT
 * Dependencies: [@types/node](https://npmjs.com/package/@types/node)

# Credits
These definitions were written by [Daniel Cassidy](https://github.com/djcsdy).
